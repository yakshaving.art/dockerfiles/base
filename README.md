[![pipeline status](https://gitlab.com/yakshaving.art/dockerfiles/base/badges/master/pipeline.svg)](https://gitlab.com/yakshaving.art/dockerfiles/base/-/commits/master)
# base

Our bonsai tree root. Alpine based, but maybe we'll have multiple, time will tell.

This image is multiarch, see os/architectures we build in .gitlab-ci.yml

This image is rebuilt weekly, and in turn, triggers the rebuilt of dependent images.
See `TRIGGERS` variable in `publish` stage of `.gitlab-ci.yml` for exact list.
